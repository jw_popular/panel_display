#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */
unsigned char *ptr1;
unsigned short *ptr2;
unsigned char *ptr_temp;

int main(int argc, char *argv[]) {
	int arrLen=4000;
	int i,j;
	
	ptr1 = (unsigned char*) malloc (arrLen * sizeof(unsigned char));
	for(i=0;i<arrLen;i+=2)
	{
	    ptr1[i]=0x41;
	    ptr1[i+1]=0x07;
    }
    #if 0
    for(j=0;j<arrLen;j++)
    {
        printf("[%02X]", ptr1[j]);
    }
    printf("\n");
    #endif
	
	ptr2 = (unsigned short*) malloc (arrLen/2 * sizeof(unsigned short));
	for(i=0;i<(arrLen/2);i++)
	{
	    ptr2[i]=0x0741;
    }
    
    ptr_temp=(unsigned char*)ptr2;
    #if 0
    for(j=0;j<(arrLen/2);j++)
    {
        printf("[%02X]", ptr2[j]&0xFF);
        printf("[%02X]", (ptr2[j]>>8)&0xFF);
    }
    printf("\n");
	#endif
	
    #if 1
    for(j=0;j<arrLen;j++)
    {
        printf("[%02X]", ptr_temp[j]);
    }
    printf("\n");
    #endif
	
	free(ptr1);
	free(ptr2);
    
    return 0;
}
